#!/bin/bash

rm $1_filesToRun.txt

for k in {'_',{0..9}}
do 
	for j in {0..9}
	do 
		echo $k$j
		for directory in $(ls -d $1/*/)
		do 
#			echo $directory
			for file in $(ls $directory*$k$j.xml)
			do
				outFile=${file%.xml}.out
				
				if [ ! -f $outFile ]
				then
					ls $file >>  $1_filesToRun.txt
				fi
			done
		done
	done
done
