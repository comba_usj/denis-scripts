import xml.etree.ElementTree as ET
import shutil
from datetime import datetime
import subprocess
import sys

print("\n === Sending Tasks to DENIS@Home === \n")

if len(sys.argv) < 4:
    print("The program needs three arguments: projectName subprojectName App_Name")
    sys.exit()

projectName = sys.argv[1] #"01_Optimization_with_different_start"
subprojectName = sys.argv[2] #"CNM_eval_0"
APP_NAME = sys.argv[3]

INPUT_FOLDER: str = '/home/boincadm/projects/denisathome/input_files'
FILE_PREFIX = APP_NAME + "_"

listWithConfigurationFilesName = projectName + "/" + subprojectName + "_filesToRun.txt"
listWithConfigurationFiles = open(listWithConfigurationFilesName, 'r')
configurationNames = listWithConfigurationFiles.readlines()
batch_time = datetime.now().strftime("%Y%m%d%H%M%S%f")
print("Date time:", batch_time)

count = 0
for configurationName in configurationNames:
    count += 1
#    if count>2:
#        break;
    configurationName = configurationName[:-1]  # Removes line break
    print(str(count) + ". " + configurationName)

    configuration = ET.parse(projectName + "/" + configurationName)
    configRoot = configuration.getroot()
    endTime = float(configRoot.find("time").text)
    dt = float(configRoot.find("dt").text)

    iterations = endTime / dt
    delayBound = 24.0 * 3600.0 * (3.0 + iterations / 2000000000.0)
    flops = 5264 * iterations # Obtained from the statistics of the platform
    maxFlops = flops * 100
    priority = 10

    configurationNameOnlyFile = configurationName.split('/')
    configurationNameOnlyFile = configurationNameOnlyFile[-1]
    print(configurationNameOnlyFile)
    nameInDENIS = FILE_PREFIX + batch_time + "_" + configurationNameOnlyFile
    shutil.copy(projectName + "/" + configurationName,
                INPUT_FOLDER + "/" + nameInDENIS)

    subprocess.call("cd " + INPUT_FOLDER + "/../ ;./bin/stage_file input_files/" + nameInDENIS, shell=True)

    subprocess.call("cd " + INPUT_FOLDER + "/../ ;./bin/create_work" +
                    " --appname " + APP_NAME +
                    " --wu_name " + nameInDENIS[:-4] +
                    " --delay_bound " + str(delayBound) +
                    " --rsc_fpops_est " + str(flops) +
                    " --rsc_fpops_bound " + str(maxFlops) +
                    " --priority " + str(priority) +
                    " --wu_template templates/MYOCYTE_in" +
                    " --result_template templates/MYOCYTE_out " +
                    # " --batch " + projectName + "-" + subprojectName + " " +
                    # " --broadcast_user 2 " + nameInDENIS, shell=True)
                    nameInDENIS, shell=True)
