#!/bin/sh

echo ' '
echo ' '
echo '============================================================'
echo '                    --- DENIS Myocyte ---                   '
echo '                                                            '
echo '                  Installation has started                  '
echo '============================================================'
echo ' '

ORIGINAL_PATH=$(pwd)
mkdir DENIS_Project
mkdir DENIS_Project/BOINC_dev
git clone http://github.com/BOINC/boinc DENIS_Project/BOINC_dev/boinc
cd DENIS_Project/BOINC_dev/boinc/mac_build
MAC_OS_X_VERSION_MAX_ALLOWED=1040
MAC_OS_X_VERSION_MIN_REQUIRED=1040
source BuildMacBOINC.sh -lib
cd ../../../
git clone https://bitbucket.org/usj_bsicos/denis-myocyte.git
cd denis-myocyte
git checkout -b develop origin/develop
mkdir cmake-macos
cd cmake-macos
/Applications/CMake.app/Contents/bin/cmake ..
make