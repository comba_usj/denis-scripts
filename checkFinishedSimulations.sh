#!/bin/bash
finished=0
total=0
output=""

echo -e "\033[1;34m\n === Status of the simulation $1 === \n\033[0m"

count=0

for d in $(ls -d $1/*/)
do
        finishedInFolder=$(ls -l $d/*.out 2>/dev/null|wc -l)
        expectedInFolder=$(ls -l $d/*.xml 2>/dev/null|wc -l)
        total=$((total+expectedInFolder))
        finished=$((finished+finishedInFolder))
	if [[ $expectedInFolder -gt 0 ]] 
	then
		count=$((count+1))
		output="$output\n$count. ${d%/}: $finishedInFolder/$expectedInFolder ($((finishedInFolder*100/expectedInFolder))%)"
	fi
	echo -e "=\c"
done

echo -e "\r\c"

echo -e $output | column -t

echo -e "\033[1;32m\nTotal:\t$finished/$total ($((finished*100/total))%)"
echo -e "\033[1;31mFaltan:\t$((total-finished))\n\033[0m"
