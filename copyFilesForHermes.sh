#!/bin/bash

originalFolder=$1
newFolder=$1\_forHermes

rm -r $newFolder
rm $newFolder.tar.bz2

mkdir $newFolder
cp $originalFolder/*.* $newFolder

for dir in $(ls -d $originalFolder/*/)
do
	newDir=${dir#$originalFolder/*}
	
	mkdir $newFolder/$newDir
	mkdir $newFolder/$newDir/Out

	echo $newDir

	for k in {'_',{0..9}}
	do
		for j in {0..9}
		do
			# echo $k$j

                        for file in $(ls $dir/*$k$j.xml)
                        do
                                outFile=${file%.xml}.out

                                if [ ! -f $outFile ]
                                then
                                        cp $file $newFolder/${file#$originalFolder/*}
                                fi
                        done
                done
        done
done

tar -cvjf $newFolder.tar.bz2 $newFolder $originalFolder-config.txt $originalFolder.sh $originalFolder.csv
